const MainPage = require("./../po/pages/mainPage.pages.js");

const mainPage = new MainPage();

describe("New Paste test", ()=>{

    beforeEach(async ()=>{
        await mainPage.open('');
        await browser.maximizeWindow();
    });

    //This test creates a dummy bin for 10 minutes
    it("New Bin Paste test 01", async()=>{

        //This section clicks on the post Box which contains the text that will be be in the pastebin
        await mainPage.postFields.item('postBox').click();
        await mainPage.postFields.item('postBox').setValue("Hello from WebDriver");

        //debug pause to verify it's done correctly
        await browser.pause(1000);

        /* This section selects the Paste Expiration time. This element is hidden, therefore we need a different approach in order to be able to select it. */
        //First we scroll until the visible element is visibile on the screen and clicks on the dropdown.
        await mainPage.postFields.pasteExpirationViewable.scrollIntoView();
        await mainPage.postFields.pasteExpirationViewable.click();

        //Then we execute commands on the Chrome's Console to make the select element visible
        await browser.execute(()=>{

            expiration = document.getElementById("postform-expiration");
            expiration.style = "width: 1px; height: 1px; visibility: visible;"
            expiration.aria_hidden = false;

        });

        //Now that the element is not hidden, we can now select by attribute 'value'
        await mainPage.postFields.pasteExpiration.selectByAttribute("value","10M");

        //This is a debug pause to verify the process is done correctly
        await browser.pause(1000);

        //Now we click on the Paste Name field input and set the input value to 'helloweb'
        await mainPage.postFields.item('pasteName').click();
        await mainPage.postFields.item('pasteName').setValue("helloweb");

        //debug pause for verification
        await browser.pause(1000);

        //now we click on the Create New Paste button
        await mainPage.postFields.newPasteBtn.click();

        //debug pause to verify the process has been done correctly
        await browser.pause(2000);

    });
})
