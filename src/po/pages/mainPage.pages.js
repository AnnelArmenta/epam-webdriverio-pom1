const NewPostFields = require("./../components/newBin/newPostBottom.component");
const NewPostBox = require("./../components/newBin/newPost.component.js");

class MainPage{

    constructor(){
        this.postBox = new NewPostBox();
        this.postFields = new NewPostFields();
    }
    async open(){
        await browser.url('');
    }
}

module.exports = MainPage;